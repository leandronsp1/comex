#!/bin/bash

rm -f /app/tmp/pids/server.pid
bundle && bundle exec rake db:migrate
exec "$@"
