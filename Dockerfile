FROM ruby AS base
WORKDIR /app

FROM base AS release
ENV RAILS_ENV=production
COPY Gemfile Gemfile.lock ./
RUN bundle install --without development test
COPY . .
RUN bundle exec rake assets:precompile
ENTRYPOINT [ "./entrypoint.sh" ]
EXPOSE 3000
CMD ["rails", "server", "-b", "0.0.0.0"]
