# comex

Development:
```bash
# Start the server at localhost:3000
$ docker compose up

# Running tests
$ docker compose run app rake db:setup
$ docker compose run app rake test
```

Provisioning K8s:
```bash
$ bash provisioning-k8s
```

Deploy:
```bash
$ bash ci-cd-k8s
```
