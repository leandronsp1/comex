class HelloController < ApplicationController
  def index
    db_time = ActiveRecord::Base.connection.execute("SELECT NOW()").first["now"]
    users = ActiveRecord::Base.connection.execute("SELECT name FROM users").to_a

    @current_time = db_time
    @users = users
  end
end
